# GraniteFone

This documentation will describe how-to make a GraniteFone and the hardware and software it consists of.

The origin of the name comes from the love of the Granite State of New Hampshire. Since there is already a product with [the name of GranitePhone](https://gadgets.ndtv.com/sikur-granitephone-3025), which is what I initially wanted to use but didn't want Indian attorneys bothering me for infringement claims, so I came up with the "GraniteFone" instead. For those who do not know, New Hampshire is the "Live Free or Die" state :)

## Let's get started

### 0 - To build your own or to buy one instead

* This documentation will explain how you can put together your own GraniteFone. 
* Since I truly believe in DIY, encouraging people to "make" things, and transparency, this documentation includes everything to enable anyone in the world to make one.
* On a limited basis, I will be offering the GraniteFone for people to purchase, so if interested please contact me and we can discuss.

I believe we are at a time when some people might seriously consider protecting themselves from the "clutches" of big ol' [Apple](https://www.nasdaq.com/market-activity/stocks/aapl) or the [G00g](https://www.nasdaq.com/market-activity/stocks/goog).

### 1 - Hardware and Operating System

In this section we discuss the hardware & the Operating System I have chosen for this phone called [GrapheneOS](https://grapheneos.org).

I chose these for the following reasons, but you can [read more](https://grapheneos.org/features) about if you'd like to.

* It is a Private and Secure version of Android
* It does not contain any tracking, unlike the stock OS on most devices
* It does not include any of the g00g's play services or store
* Updates download and can be applied quickly with a reboot, unlike some of the other versions of Android
* Has an easy-to-use built-in firewall if you need to block an application's data/WiFi connectivity
* The [GrapheneOS features and optimizations](https://grapheneos.org/features) are very impressive
* Only Pixel phones have substantial hardware-based support for enhancing the security of the phone's disk encryption implementation [Source](https://grapheneos.org/faq#encryption)
* It does not run applications as root for [these reasons](https://madaidans-insecurities.github.io/android.html#rooting)
  * If you have not heard of Madaidan, then please take note that he knows his shhhht and enjoys sharing his findings :)

The GrapheneOS project suggests [these phones](https://grapheneos.org/faq#recommended-devices) for the best long term support, but since they are not in everyone's budgets you can use a lower Pixel, such as a 3 or 3a, but it won't be supported at some point with this project as has happened to the Pixel 2 and others. 

* Please always check the [unsupported devices](https://grapheneos.org/faq#which-legacy-devices) list here, so please DO NOT buy one on this list as it won't work as a GraniteFone.

### 2 - Install GrapheneOS

[Follow these instructions](https://grapheneos.org/install/) from the GrapheneOS devs to install it on your chosen Pixel phone.

* I have not tried the [web-based installer](https://grapheneos.org/install/web) yet, but upstream recommends it for most users. I would suggest you give it a try as it is easier to do.
* I'm a command line person, so I chose that option. Both myself and upstream suggest this for technical people only.

### 3 - Install those Apps

First off you will have [F-Droid store](https://f-droid.org) already installed as part of the above installation.

A GraniteFone should only have applications that use the least amount of permissions.

These are all installable via the F-Droid store. Below take note to refer to E2EE as "End-to-End Encryption" & here's the list:

| App name   | What it does                             |
|------------|:----------------------------------------:|
| andOTP     | Used for 2-Factor Authentication (if needed) | 
| antennaPod | A free and secure podcasting application | 
| Element    | The [Matrix protocol phone app](https://en.wikipedia.org/wiki/Matrix_(protocol)) for the next generation of secure E2EE decentralized messaging | 
| InviZible Pro | An app to use TOR, I2P, or dnscrypt-proxy without needing root access | 
| Jitsi Meet | Instant video conferences efficiently adapting to your scale because we all need to start looking at this great positive alternative to the un-ethical company Zoom |
| NewPipe    | An awesome safer YouTube app which (1) lets you download the video/music onto your phone from the app (2) can stream other services, like PeerTube and SoundCloud | 
| Port Authority | App to scan the network your phone is connected to for other hosts | 
| Scrambled Exif | Remove the metadata from your pictures before sharing them |
| SecScanQR  | The QR code scanner/generator that cares about your privacy | 
| Silence    | Encrypted SMS/MMS conversations made easy!  | 
| Simple File Manager | The name says it all            | 
| Simple Flashlight | The name says it all              | 
| Simple Gallery | The name says it all                 | 
| Tusky      | A Mastodon protocol phone app, which is a decentralized social media where no one is in charge! | 
| VLC        | The swiss-army knife of media players as this plays everything |

Optional recommendations "as needed" are the following apps too:
* Bromite - a privacy oriented Chromium clone
* Etesync - an app for a subscription-based (or self-hosted if your're a tech) secure E2EE service which is able to securely backup your contacts and calendar information
* JuiceSSH - if you need to SSH into a computer then this works pretty well for it
* Nextcloud apps - if you want to use a Nextcloud server, then these are available in the F-Droid store
* Stealth - an account-free, privacy-oriented, and feature-rich Reddit client
* Termux - if you need command line Linux things, then look into this. I don't use it very often but it's available.
* Monerujo - best phone Monero wallet, but only appears on F-Droid if you add a custom repository ([Instructions here](https://f-droid.monerujo.io/)).

If you do actually need something from the G00g's app store, then you can install Aurora Store on F-Droid as a "safe" front-end as you can select to use anon accounts created by this app's dev.

### 4 - Application instruction guides

* [How to get started with Element Matrix client](https://codeberg.org/jahway603/matrix-tldr-howto)
* 

### Future plans

* Create additional user-friendly guides to help people better utilize their new GraniteFone.
* To make a version with Linux running on it, like a [PinePhone](https://www.pine64.org/pinephone/).

### About Me

I have been modifying, customizing, and hacking Android phones since 2009 (the last 12 years). The GrapheneOS experience has been so awesome that I wanted to bring this experience to others. I hope you enjoy your phone whether you DIY or get one from me.

### Contact Info

If you know me personally, then just ask me about these phones.

If you are interwebz only, then [find me on the Matrix](https://matrix.to/#/@jahway603:privacytools.io).

And please NO Signal because they implemented their own ishcoin behind everyone's back during most of last year, so no more trust for them from me and many others.

By visiting the [original page about it on Archive.org](https://web.archive.org/web/20201127142851/https://mobilecoin.foundation/about), as they have recently removed it from their site, you can see the following below. I do not agree with this and have stopped using Signal. Everyone can make their own decision but I recommend to ditch Signal and to embrace the Matrix.

```
At MobileCoin, we believe governments have a legitimate interest in regulating the economic lives of their citizens.
```

#### License

[GPLv3](LICENSE)
